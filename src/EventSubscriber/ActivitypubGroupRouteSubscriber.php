<?php

namespace Drupal\activitypub_group\EventSubscriber;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber.
 */
class ActivitypubGroupRouteSubscriber extends RouteSubscriberBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs an ActivitypubRouteSubscriber object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alteroRoutes(RouteCollection $collection) {

    foreach ($collection->all() as $route_id => $route) {
      // Hide taxonomy pages from unprivileged users.
      if (strpos($route->getPath(), '/user//taxonomy/term') === 0) {
        // TODO: Veure si es activitypub.user.self i si té type =group, si el té modifica el path per no tenir la id de l'usuari.
        $route->setRequirement('_role', 'administrator');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $undefined_lang = $this->languageManager->getLanguage(Language::LANGCODE_NOT_SPECIFIED);

    // All activitypub endpoints that needs to be canonical
    $activitypub_endpoints = [
      'entity.activitypub_activity.canonical',
      'activitypub.user.self',
      'activitypub.user.self.json',
      'activitypub.node.json',
      'activitypub.comment.json',
      'activitypub.inbox',
      'activitypub.outbox',
      'activitypub.followers',
      'activitypub.following',
    ];
    // Set them all as undefined lang to not alter the path 
    // by language in any way.

    foreach ($activitypub_endpoints as $endpoint) {
      if ($route = $collection->get($endpoint)) {
        //print_r($endpoint);//;
        print_r($route->getOptions());
        if ($route->getOption('actor_type') == 'Person' ){//|| $endpoint == 'activitypub.user.self') {
          print_r($route->getOption('actor_type'));
          print_r($route->getOptions());
          $route->setPath('/group/activitypub/{activitypub_actor}');
        }
        $route->setOption('default_url_options', [
          'language' => $undefined_lang
        ]);
      }
    }
  }


  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = parent::getSubscribedEvents();

    // Use a lower priority than \Drupal\views\EventSubscriber\RouteSubscriber
    // to ensure the requirement will be added to its routes.
    //$events[RoutingEvents::ALTER] = ['onAlterRoutes', -300];

    return $events;
  }

}
