<?php

namespace Drupal\activitypub_group;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\group\GroupMembershipLoaderInterface;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupRelationship;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\activitypub\Services\ActivityPubProcessClientInterface;
use Drupal\activitypub\Services\ActivityPubUtilityInterface;
use Drupal\Core\Url;

/**
 * Service description.
 */
class ActivitypubGroupFormAlter {
  use StringTranslationTrait;

  /**
   * The group_relation_type.manager service.
   *
   * @var \Drupal\group\Plugin\Group\Relation\GroupRelationTypeManagerInterface
   */
  protected $manager;

  /**
   * The group.membership_loader service.
   *
   * @var \Drupal\group\GroupMembershipLoaderInterface
   */
  protected $membershipLoader;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The ActivityPub Actor storage.
   *
   * @var \Drupal\activitypub\Entity\Storage\ActivityPubActorStorageInterface
   */
  protected $actorStorage;

  /**
   * The ActivityPub type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $typeStorage;

  /**
   * The ActivityPub Activity  storage.
   *
   * @var \Drupal\activitypub\Entity\Storage\ActivityPubActivityStorageInterface
   */
  protected $activityStorage;

  /**
   * The process client.
   *
   * @var \Drupal\activitypub\Services\ActivityPubProcessClientInterface
   */
  protected $activityPubProcessClient;

  /**
   * The ActivityPub utility service.
   *
   * @var \Drupal\activitypub\Services\ActivityPubUtilityInterface
   */
  protected $activityPubUtility;

  /**
   * Constructs an ActivitypubGroupFormAlter object.
   *
   * @param \Drupal\group\Plugin\Group\Relation\GroupRelationTypeManagerInterface $manager
   *   The group_relation_type.manager service.
   * @param \Drupal\group\GroupMembershipLoaderInterface $membership_loader
   *   The group.membership_loader service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\activitypub\Services\ActivityPubUtilityInterface $activitypub_utility
   *   The ActivityPub utility service.
   * @param \Drupal\activitypub\Services\ActivityPubProcessClientInterface $activitypub_process_client
   *   The ActivityPub process client service.
   */
  public function __construct(GroupRelationTypeManagerInterface $manager, GroupMembershipLoaderInterface $membership_loader, EntityTypeManagerInterface $entity_type_manager, ActivityPubUtilityInterface $activitypub_utility, ActivityPubProcessClientInterface $activitypub_process_client) {
    $this->manager = $manager;
    $this->membershipLoader = $membership_loader;
    $this->entityTypeManager = $entity_type_manager;
    $this->activityPubUtility = $activitypub_utility;
    $this->activityPubProcessClient = $activitypub_process_client;
    $this->actorStorage = $this->entityTypeManager->getStorage('activitypub_actor');
    $this->activityStorage = $this->entityTypeManager->getStorage('activitypub_activity');
    $this->typeStorage = $this->entityTypeManager->getStorage('activitypub_type');
  }

  /**
   * Adds the ActivityPub settings form element.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param \Drupal\Core\Entity\EntityInterface $group
   */
  public function addActivityPubOutboxFormElement(array &$form, FormStateInterface $form_state, EntityInterface $entity, EntityInterface $group) {
    $actor = $this->actorStorage->loadActorByEntityIdAndType($group->id(), 'group');
    // Actor href. 
/*
 activitypub.user.self:
  path: '/{entity_type}/{eid}/activitypub/{activitypub_actor}'
  defaults:
    _controller: '\Drupal\activitypub\Controller\UserController::self'
    _title: 'Self'
  methods: [GET]
  requirements:
    _entity_access: 'entity.view'
    _custom_access: '\Drupal\activitypub\Controller\UserController::actorUserCheck'
  options:
    parameters:
      activitypub_actor:
        type: 'activitypub_actor'
      eid:
        type: entity:{entity_type}
      entity_type:
        type: string
 */

    $actor_href = Url::fromRoute('activitypub.user.self', ['eid' => $actor->get('uid')->entity->id(), 'activitypub_actor' => $actor->getName(), 'entity_type' => 'group'], ['absolute' => TRUE])->toString();
    dpm($actor_href);

    if (!$actor) {
      return;
    }

    $options = [];
    $outbox_selected = [];
    /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface[] $outboxEntities */
   if ($entity->id()) {
      $outboxEntities = $this->activityStorage->loadByProperties(['entity_type_id' => $entity->getEntityTypeId(), 'entity_id' => $entity->id(), 'collection' => 'outbox', 'actor' => $actor_href ]);
      foreach ($outboxEntities as $outboxEntity) {
        $outbox_selected[$outboxEntity->getConfigID()] = $outboxEntity->getConfigID();
      }
    }

    $typeEntities = $this->typeStorage->loadByProperties(['status' => TRUE, 'plugin.configuration.target_entity_type_id' => $entity->getEntityTypeId(), 'plugin.configuration.target_bundle' => $entity->bundle()]);
    if (!$typeEntities) {
      return;
    }

    foreach ($typeEntities as $entity) {
      if (!isset($outbox_selected[$entity->id()])) {
        $options[$entity->id()] = $entity->label();
      }
    }
    
    if (!is_array($form['activitypub_groups'])) $form['activitypub_groups'] = [
      '#type' => 'container',
      '#title' => $this->t('ActivityPub outbox for groups'),
      '#group' => 'advanced',
      '#tree' => TRUE
      ];

    $form['activitypub_groups']['group_' . $group->id()] = [
      '#type' => 'details',
      '#title' => $this->t('ActivityPub outbox for group @group_label', ['@group_label' => $group->label()]),
      '#group' => 'advanced',
      '#tree' => TRUE,
    ];

    $form['activitypub_groups']['group_' . $group->id()]['activitypub_group_actor'] = [
      '#type' => 'value',
      '#value' => $actor,
    ];

    if ($options) {
      $options = ['' => $this->t('- Select -')] + $options;
      $form['activitypub_groups']['group_' . $group->id()]['activitypub_group_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Type'),
        '#options' => $options,
      ];

      $form['activitypub_groups']['group_' . $group->id()]['activitypub_group_to'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Send to'),
        '#description' => $this->t('Add URL\'s of (remote) users line per line. Only add those who do not follow you.'),
        '#default_value' => !empty($_GET['to']) ? $_GET['to'] : '',
      ];

      foreach (array_keys($form['actions']) as $action) {
        if ($action === 'submit' && isset($form['actions'][$action]['#type']) && $form['actions'][$action]['#type'] === 'submit') {
        $form['actions'][$action]['#submit'][] = [$this, 'submitGroupOutboxForm'];
        }
      }
    }
    else {
      $form['activitypub_groups']['group_' . $group->id()]['activitypub_group_sent'] = [
        '#markup' => $this->t('This post has been sent to the outbox.'),
      ];
    }
  }

  /**
   * Submit outbox form.
   *
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitGroupOutboxForm($form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $form_state->getFormObject()->getEntity();
    foreach ($form_state->getValue('activitypub_groups') as $group => $activity_definition) {
      if ($type = $activity_definition['activitypub_group_type']) {
        /** @var \Drupal\activitypub\Entity\ActivityPubTypeInterface $activityPubType */
        $activityPubType = $this->typeStorage->load($type);

        $values = [
          'collection' => 'outbox',
          'config_id' => $type,
          'type' => $activityPubType->getPlugin()['configuration']['activity'],
          'uid' => $entity->getOwnerId(),
          'actor' => $this->activityPubUtility->getActivityPubID($activity_definition['activitypub_group_actor']),
          'entity_type_id' => $entity->getEntityTypeId(),
          'entity_id' => $entity->id(),
          'processed' => FALSE,
          'status' => TRUE,
          'direct' => FALSE,
        ];

        if ($to = $activity_definition['activitypub_group_to']) {
          $values['to'] = $to;
        }

        /** @var \Drupal\activitypub\Entity\ActivityPubActivityInterface $activity */
        $activity = $this->activityStorage->create($values);
        $doSave = TRUE;
        $activity->preOutboxSave($entity, $activityPubType, $doSave);
        if ($doSave) {
          $activity->save();
          $this->activityPubProcessClient->createQueueItem($activity);
          Cache::invalidateTags(['user:' . $entity->getOwnerId()]);
        }
      }
    }
  }
}
