<?php

namespace Drupal\activitypub_group\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\activitypub\Services\ActivityPubFormAlterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a Activitypub group form.
 */
class ActivitypubGroupForm extends FormBase {

  /**
   * The ActivityPub form alter service.
   *
   * @var \Drupal\activitypub\Services\ActivityPubFormAlterInterface
   */
  protected $activityPubFormAlter;

  /**
   * UserForm constructor
   *
   * @param \Drupal\activitypub\Services\ActivityPubFormAlterInterface $activitypub_form_alter
   */
  public function __construct(ActivityPubFormAlterInterface $activitypub_form_alter) {
    $this->activityPubFormAlter = $activitypub_form_alter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('activitypub.form_alter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'activitypub_group_activitypub_group';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, GroupInterface $group = NULL) {
    $this->activityPubFormAlter->addActivityPubSettingsFormElement($form, $form_state, $group, 'group');

    return $form;
  }

  /**
   * Check that the actor points to the user.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function currentUserCheck(GroupInterface $group) {
    if ($this->currentUser()) {
      return AccessResult::allowedIfHasPermission($this->currentUser(),'allow users to enable activitypub');
    }

    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
